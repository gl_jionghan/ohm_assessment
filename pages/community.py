from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

@app.route('/community', methods=['GET'])
def community():
	users = User.get_five_most_rencent_user()
	for user in users:
		user['phone'] = User.get_phone_number_by_id(user['id'])
		user['loc'] = User.get_location_by_id(user['id'])	
	args = {
		'users': users
	}
	return render_template("community.html", **args)