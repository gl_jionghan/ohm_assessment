""" Users data update

Revision ID: 0000001
Revises: 0000000
Create Date: 2017-12-31 10:50:52.896256

"""

# revision identifiers, used by Alembic.

revision = '00000001'
down_revision = '00000000'

from alembic import op

def upgrade():
    op.execute('''UPDATE user SET point_balance = 1000 WHERE user_id = 1 
    ''')
    op.execute('''INSERT INTO rel_user
    	(user_id, rel_lookup, attribute)
    	VALUES
    	(2, 'LOCATION', 'USA')
    ''')
    op.execute('''UPDATE user SET tier = 'Silver' WHERE user_id = 3
    ''')	

def downgrade():
	op.execute('''UPDATE user SET point_balance = 0 WHERE user_id = 1
	''')
    op.execute('''DELETE FROM rel_user WHERE user_id = 2
    ''')
    op.execute('''UPDATE user SET tier = 'Carbon' WHERE user_id = 3
    ''')